ARG CLI_IMAGE
FROM ${CLI_IMAGE} as cli

FROM amazeeio/nginx-drupal

COPY --from=cli /app /app

# Define where the Drupal Root is located
ENV WEBROOT=web

COPY lagoon/drupal.conf /etc/nginx/conf.d/app.conf

COPY lagoon/location_prepend_proxy.conf /etc/nginx/conf.d/drupal/location_drupal_prepend_proxy.conf
COPY lagoon/location_prepend_proxy.conf /etc/nginx/conf.d/drupal/location_php_prepend_proxy.conf