<?php
/**
 * @file
 * amazee.io Drupal 8 all environment configuration file.
 *
 * This file should contain all settings.php configurations that are needed by all environments.
 *
 * It contains some defaults that the amazee.io team suggests, please edit them as required.
 */

// Defines where the sync folder of your configuration lives. In this case it's inside
// the Drupal root, which is protected by amazee.io nginx configs, so it cannot be read
// via the browser. If your Drupal root is inside a subfolder (like 'web') you can put the config
// folder outside this subfolder for an advanced security measure: '../config/sync'.
$config_directories[CONFIG_SYNC_DIRECTORY] = '../config/sync';

/*
 * proxypass - tweak 
 */
if (getenv('LAGOON') && isset($GLOBALS['request'])) {
    $scriptName = $GLOBALS['request']->server->get('SCRIPT_NAME');
    $requestUri = $GLOBALS['request']->server->get('REQUEST_URI');
  
    $rewritePaths = [
      '/' . getenv('urlstrategy_namespace'),
    ];
  
    foreach ($rewritePaths as $item) {
      if ($requestUri == $item || $requestUri == $item . '/' || strpos($requestUri, $item) === 0) {
        $GLOBALS['request']->server->set('SCRIPT_NAME', $item . $scriptName);
  
        if ($requestUri == $item) {
          $GLOBALS['request']->server->set('REQUEST_URI', $item . '/');
        }
        break;
      }
    }
  }